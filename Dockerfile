FROM alpine

COPY patches/disable-hostname-validation.patch /tmp

RUN apk add --no-cache lua-turbo && \
	patch -p0 -i /tmp/disable-hostname-validation.patch 

